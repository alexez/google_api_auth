cmake_minimum_required(VERSION 3.16)
project(google_api_auth_example C)

set(CMAKE_C_STANDARD 11)

add_executable(google_api_auth_example main.c)

target_link_libraries(google_api_auth_example PRIVATE google_api_auth)