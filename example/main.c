#include "google_api_auth/auth.h"
#include <stdio.h>
#include <stdlib.h>

int main() {
    unsigned size = 3;
    ScopesArray array;
    array.collection = malloc(sizeof(const char *) * size);
    array.size = size;
    array.collection[0] = "ololo";
    array.collection[1] = "alala";
    array.collection[2] = "elele";

    authenticate_app(array, "", "");

    return 0;
}
