#pragma once

typedef struct ScopesArray {
    const char **collection;
    unsigned size;
} ScopesArray;

int authenticate_app(ScopesArray scopes, const char *client_id, const char *client_secret);
