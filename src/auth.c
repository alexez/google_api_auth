#include "google_api_auth/auth.h"

#include "request_sender.h"
#include "simple_server.h"
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

typedef struct PreparedScopesResult {
    char *scopes_string;
    unsigned length;
} PreparedScopesResult;

int prepare_scopes_string(ScopesArray scopes, PreparedScopesResult *result) {
    unsigned result_length = 0;
    for (unsigned i = 0; i < scopes.size; ++i) {
        result_length += strlen(scopes.collection[i]) + 1;
    }

    result->scopes_string = malloc(result_length);

    for (unsigned i = 0, c = 0; i < scopes.size; ++i) {
        unsigned length = strlen(scopes.collection[i]);
        strncpy(result->scopes_string + c, scopes.collection[i], length);
        c += length;
        result->scopes_string[c] = ' ';
        ++c;
    }

    result->scopes_string[result_length - 1] = '\0';
    result->length = result_length;

    return 0;
}

int prepare_request_url(ScopesArray scopes, const char *client_id, char **result) {
    const char *url_template = "https://accounts.google.com/o/oauth2/v2/auth?"
                               "response_type=code&"
                               "scope=%s&"
                               "redirect_uri=%s&"
                               "client_id=%s";

    PreparedScopesResult prepared_scopes;
    prepare_scopes_string(scopes, &prepared_scopes);

    fprintf(stdout, "prepared scopes: \"%s\"\n", prepared_scopes.scopes_string);

    const char *redirect_uri = "http://127.0.0.1:20200";

    unsigned url_length = strlen(url_template) + strlen(prepared_scopes.scopes_string) + strlen(redirect_uri) + strlen(client_id) + 1;

    *result = malloc(url_length);
    sprintf(*result, url_template, prepared_scopes.scopes_string, redirect_uri, client_id);

    free(prepared_scopes.scopes_string);

    return 0;
}

bool stop = false;

static RequestHandlerResult request_handler(const char *path, const KeyValueArray keyValueArray, char **response) {
    bool isRootPath = false;
    if (strcmp(path, "/") == 0) {
        isRootPath = true;
    }

    bool hasCode = false;
    bool hasError = false;
    for (int i = 0; i < keyValueArray.size; ++i) {
        if (strcmp(keyValueArray.collection[i].key, "code") == 0) {
            hasCode = true;
        } else if (strcmp(keyValueArray.collection[i].key, "error") == 0) {
            hasError = true;
        }
        fprintf(stdout, "handle argument[%d] key:%s\n", i, keyValueArray.collection[i].key);
        fprintf(stdout, "handle argument[%d] value:%s\n", i, keyValueArray.collection[i].value);
    }

    if (isRootPath && (hasCode || hasError)) {
        stop = true;

        *response = malloc(15);
        if (hasCode) {
            strncpy(*response, "has code", 15);
        } else {
            strncpy(*response, "has error", 15);
        }
    } else {
        *response = malloc(15);
        strncpy(*response, "hello, world!", 15);
    }

    return REQUEST_HANDLER_RESULT_OK;
}

int authenticate_app(ScopesArray scopes, const char *client_id, const char *client_secret) {
    //    ServerData *server_data;
    //    ServerResult result = start_server(&server_data, &request_handler);
    //    if (result != SERVER_RESULT_OK) {
    //        return 1;
    //    }
    //
    //    while (!stop) {
    //        usleep(2000);
    //    }
    //
    //    stop_server(&server_data);

    const char *authorization_code = "";

    KeyValueArray keyValueArray = create_key_value_array(5);
    add_key_value_to_array(&keyValueArray, "client_id", "client_id");
    add_key_value_to_array(&keyValueArray, "client_secret", "client_secret");
    add_key_value_to_array(&keyValueArray, "code", authorization_code);
    add_key_value_to_array(&keyValueArray, "grant_type", "authorization_code");
    add_key_value_to_array(&keyValueArray, "redirect_uri", "http://127.0.0.1:20200");

    send_request(REQUEST_TYPE_POST, "https://oauth2.googleapis.com/token", keyValueArray);

    free_key_value_array(keyValueArray);

    //    char *url;
    //    prepare_request_url(scopes, client_id, &url);
    //    fprintf(stdout, "prepared request url: \"%s\"\n", url);

    return 0;
}
