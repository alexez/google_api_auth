#pragma once

#include <stdlib.h>
#include <string.h>

typedef struct KeyValue {
    char *key;
    char *value;
} KeyValue;

typedef struct KeyValueArray {
    KeyValue *collection;
    unsigned size;
    unsigned current;
} KeyValueArray;

static inline KeyValueArray create_key_value_array(unsigned size) {
    KeyValueArray keyValueArray;

    keyValueArray.collection = (size != 0) ? malloc(sizeof(KeyValue) * size) : NULL;
    keyValueArray.size = size;
    keyValueArray.current = 0;

    return keyValueArray;
}

static inline void add_key_value_to_array_ranged(KeyValueArray *array, const char *key_begin, const char *key_end, const char *value_begin, const char *value_end) {
    unsigned key_length = key_end - key_begin;
    unsigned value_length = value_end - value_begin;

    char *key_buffer = malloc(key_length + 1);
    char *value_buffer = malloc(value_length + 1);

    memcpy(key_buffer, key_begin, key_length);
    key_buffer[key_length] = '\0';

    memcpy(value_buffer, value_begin, value_length);
    value_buffer[value_length] = '\0';

    array->collection[array->current].key = key_buffer;
    array->collection[array->current].value = value_buffer;

    ++array->current;
}

static inline void add_key_value_to_array(KeyValueArray *array, const char *key, const char *value) {
    unsigned key_length = strlen(key);
    unsigned value_length = strlen(value);

    add_key_value_to_array_ranged(array, key, key + key_length, value, value + value_length);
}

static inline void free_key_value_array(KeyValueArray keyValueArray) {
    for (int i = 0; i < keyValueArray.size; ++i) {
        free(keyValueArray.collection[i].key);
        free(keyValueArray.collection[i].value);
    }
    free(keyValueArray.collection);

    keyValueArray.size = 0;
    keyValueArray.current = 0;
}
