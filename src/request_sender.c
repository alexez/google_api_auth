#include "request_sender.h"

#include <curl/curl.h>

void send_request(RequestType requestType, const char *url, KeyValueArray fields) {
    CURL *curl;
    CURLcode res;

    curl = curl_easy_init();
    if (!curl) {
        fprintf(stderr, "curl_easy_init failed\n");
        // return 3;
    }
    struct curl_slist *chunk = NULL;
    chunk = curl_slist_append(chunk, "Accept:");

    curl_easy_setopt(curl, CURLOPT_HTTPHEADER, chunk);
    curl_easy_setopt(curl, CURLOPT_URL, url);

    unsigned estimated_post_fields_length = 0;
    for (unsigned i = 0; i < fields.size; ++i) {
        estimated_post_fields_length += strlen(fields.collection[i].key) + strlen(fields.collection[i].value) + 1 + 1;
    }
    char *post_fields = malloc(estimated_post_fields_length * 3);
    {
        char *current = post_fields;
        for (unsigned i = 0; i < fields.size; ++i) {
            char *escaped_key = curl_easy_escape(curl, fields.collection[i].key, 0);
            strcpy(current, escaped_key);
            current += strlen(escaped_key);
            curl_free(escaped_key);

            *current++ = '=';

            char *escaped_value = curl_easy_escape(curl, fields.collection[i].value, 0);
            strcpy(current, escaped_value);
            current += strlen(escaped_value);
            curl_free(escaped_value);

            if (i != fields.size - 1) {
                *current++ = '&';
            }
        }
        *current = '\0';
    }
    unsigned post_fields_length = strlen(post_fields);
    fprintf(stdout, "body:\n %s\n", post_fields);

    curl_easy_setopt(curl, CURLOPT_POSTFIELDS, post_fields);
    curl_easy_setopt(curl, CURLOPT_POSTFIELDSIZE, post_fields_length);

    curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);

    res = curl_easy_perform(curl);
    if (res != CURLE_OK) {
        fprintf(stderr, "curl_easy_perform() failed: %s\n", curl_easy_strerror(res));
    }

    free(post_fields);

    curl_easy_cleanup(curl);

    curl_slist_free_all(chunk);
}
