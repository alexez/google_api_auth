#pragma once

#include "helper.h"

typedef enum RequestType
{
    REQUEST_TYPE_GET,
    REQUEST_TYPE_POST,
} RequestType;

void send_request(RequestType requestType, const char* url, KeyValueArray fields);
