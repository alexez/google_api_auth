#include "simple_server.h"

#include <assert.h>
#include <netinet/in.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/prctl.h>
#include <sys/socket.h>
#include <unistd.h>

#define SERVER_PORT 20200

typedef struct ServerData {
    int socked_file_descriptor;
    struct sockaddr_in socket_address;
    pthread_t thread;
    int thread_stop;
    get_request_handler_func get_request_handler;
} ServerData;

ServerResult create_file_descriptor(int *result) {
    *result = socket(AF_INET, SOCK_STREAM, 0);
    if (*result < 0) {
        fprintf(stderr, "socket failed\n");
        return SERVER_RESULT_SOCKET_CREATION_FAILED;
    }
    return SERVER_RESULT_OK;
}

ServerResult bind_address(int socket_file_descriptor, struct sockaddr_in *address) {
    memset(address, 0, sizeof(*address));
    address->sin_family = AF_INET;
    address->sin_addr.s_addr = htonl(INADDR_ANY);
    address->sin_port = htons(SERVER_PORT);
    if (bind(socket_file_descriptor, (struct sockaddr *)address, sizeof(*address)) < 0) {
        fprintf(stderr, "bind failed\n");
        return 2;
    }
    return SERVER_RESULT_OK;
}

ServerResult start_listening(int socket_file_descriptor) {
    if (listen(socket_file_descriptor, 3) < 0) {
        fprintf(stderr, "listen failed\n");
        return 3;
    }
    return SERVER_RESULT_OK;
}

const char *get_request_url_path(const char *request_url_begin, const char *request_url_end) {
    const char *request_url_path_end = strchr(request_url_begin, '?');
    if (request_url_path_end == NULL) {
        request_url_path_end = request_url_end;
    }

    char *request_url_path = malloc(request_url_path_end - request_url_begin);
    strncpy(request_url_path, request_url_begin, request_url_path_end - request_url_begin);
    fprintf(stdout, "request url path:%s\n", request_url_path);

    return request_url_path;
}

KeyValueArray get_arguments_array(const char *request_url_begin, const char *request_url_end) {
    KeyValueArray keyValueArray;
    keyValueArray.collection = NULL;
    keyValueArray.size = 0;

    const char *request_url_path_end = strchr(request_url_begin, '?');
    const char *request_url_arguments_begin = request_url_path_end + 1;
    if (request_url_arguments_begin < request_url_end) {
        unsigned keys = 1;
        const char *c = request_url_arguments_begin;
        while (c != request_url_end) {
            if (*(c++) == '&') {
                ++keys;
            }
        }

        keyValueArray = create_key_value_array(keys);

        const char *current_begin = request_url_arguments_begin;
        const char *current_end = strchr(request_url_begin, '&');
        unsigned index = 0;
        while (1) {
            if (current_end == NULL) {
                current_end = request_url_end;
            }
            char *equals = strchr(current_begin, '=');

            unsigned key_length = equals - current_begin;
            unsigned value_length = current_end - equals - 1;

            const char *key_begin = current_begin;
            const char *key_end = key_begin + key_length;
            const char *value_begin = equals + 1;
            const char *value_end = value_begin + value_length;
            add_key_value_to_array_ranged(&keyValueArray, key_begin, key_end, value_begin, value_end);
            fprintf(stdout, "request argument[%d]: { %s, %s }\n", index, keyValueArray.collection[keyValueArray.current - 1].key, keyValueArray.collection[keyValueArray.current - 1].value);

            if (current_end == request_url_end) {
                break;
            }

            current_begin = current_end + 1;
            current_end = strchr(current_begin, '&');
            ++index;
        }
    }
    return keyValueArray;
}

typedef enum ResultCode {
    RESULT_CODE_OK = 200,
    RESULT_CODE_BAD_REQUEST = 400,
} ResultCode;

static const char *result_code_string_ok = "OK";
static const char *result_code_string_bad_request = "Bad Request";

char *compose_result(ResultCode code, const char *body) {
    const char *result_template = "HTTP/1.1 %d %s\n"
                                  "Content-Type: text/plain\n"
                                  "Content-Length: %d\n"
                                  "\n"
                                  "%s";

    unsigned result_code;
    const char *result_code_string;
    switch (code) {
        case RESULT_CODE_OK:
            result_code = 200;
            result_code_string = result_code_string_ok;
            break;
        case RESULT_CODE_BAD_REQUEST:
            result_code = 400;
            result_code_string = result_code_string_bad_request;
            break;
        default:
            assert(!"invalid result code enum value");
    }

    unsigned body_length = strlen(body);
    unsigned result_length = strlen(result_template) + 3 + strlen(result_code_string) + 6 + body_length;
    char *result = malloc(result_length);

    sprintf(result, result_template, result_code, result_code_string, body_length, body);

    return result;
}

static void *handle_request_routine(void *arg) {
    prctl(PR_SET_NAME, "server thread", 0, 0, 0);
    ServerData *data = arg;

    struct sockaddr *address_ptr = (struct sockaddr *)&data->socket_address;
    socklen_t address_size = sizeof(*address_ptr);

    while (data->thread_stop == 0) {
        int socket_descriptor = accept(data->socked_file_descriptor, address_ptr, &address_size);
        if (socket_descriptor < 0) {
            fprintf(stderr, "accept failed\n");
            continue;
        }

        char buffer[1024];
        int read_result = read(socket_descriptor, buffer, 1024);
        if (read_result < 0) {
            fprintf(stderr, "read failed\n");
            continue;
        }

        if (strncmp(buffer, "GET", 3) != 0) {
            char *error_result = compose_result(RESULT_CODE_BAD_REQUEST, "only GET requests are supported");
            int write_result = write(socket_descriptor, error_result, strlen(error_result));
            free(error_result);
            if (write_result < 0) {
                fprintf(stderr, "write failed\n");
                continue;
            }
        }

        char *request_url_begin = buffer + 4;
        char *request_url_end = strchr(request_url_begin, ' ');

        const char *request_url_path = get_request_url_path(request_url_begin, request_url_end);
        KeyValueArray keyValueArray = get_arguments_array(request_url_begin, request_url_end);

        char *response;
        RequestHandlerResult handler_result = data->get_request_handler(request_url_path, keyValueArray, &response);

        free_key_value_array(keyValueArray);

        if (handler_result == REQUEST_HANDLER_RESULT_OK || handler_result == REQUEST_HANDLER_RESULT_BAD) {
            ResultCode result_code = handler_result == REQUEST_HANDLER_RESULT_OK ? RESULT_CODE_OK : RESULT_CODE_BAD_REQUEST;
            char *result = compose_result(result_code, response);
            free(response);
            int write_result = write(socket_descriptor, result, strlen(result));
            free(result);
            if (write_result < 0) {
                fprintf(stderr, "write failed\n");
                continue;
            }
        }

        close(socket_descriptor);
    }

    return NULL;
}

ServerResult start_server(ServerData **server_data, get_request_handler_func get_request_handler) {
    *server_data = malloc(sizeof(ServerData));
    ServerData *data = *server_data;

    data->get_request_handler = get_request_handler;

    int result;

    result = create_file_descriptor(&data->socked_file_descriptor);
    if (result != 0) {
        return result;
    }

    result = bind_address(data->socked_file_descriptor, &data->socket_address);
    if (result != 0) {
        return result;
    }

    result = start_listening(data->socked_file_descriptor);
    if (result != 0) {
        return result;
    }

    data->thread_stop = 0;
    result = pthread_create(&data->thread, NULL, handle_request_routine, data);
    if (result != 0) {
        fprintf(stderr, "thread create failed\n");
        return SERVER_RESULT_THREAD_CREATION_FAILED;
    }

    return SERVER_RESULT_OK;
}

ServerResult stop_server(ServerData **server_data) {
    ServerData *data = *server_data;

    data->thread_stop = 1;
    pthread_cancel(data->thread);
    pthread_join(data->thread, NULL);

    free(data);

    return SERVER_RESULT_OK;
}
