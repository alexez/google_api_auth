#pragma once

#include "helper.h"

typedef struct ServerData ServerData;

typedef enum RequestHandlerResult {
    REQUEST_HANDLER_RESULT_OK,
    REQUEST_HANDLER_RESULT_BAD,
    REQUEST_HANDLER_RESULT_SKIP,
} RequestHandlerResult;

typedef RequestHandlerResult (*get_request_handler_func)(const char *path, const KeyValueArray keyValueArray, char **response);

typedef enum ServerResult {
    SERVER_RESULT_OK = 0,
    SERVER_RESULT_SOCKET_CREATION_FAILED = 1,
    SERVER_RESULT_THREAD_CREATION_FAILED = 2,
} ServerResult;

ServerResult start_server(ServerData **server_data, get_request_handler_func get_request_handler);
ServerResult stop_server(ServerData **server_data);